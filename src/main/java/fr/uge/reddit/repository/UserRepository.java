package fr.uge.reddit.repository;

import fr.uge.reddit.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    @Query("SELECT u FROM User u LEFT JOIN fetch u.posts child LEFT JOIN FETCH child.scores LEFT JOIN FETCH child.comments WHERE u.username = :username")
    Optional<User> findByUsername(String username);
}
