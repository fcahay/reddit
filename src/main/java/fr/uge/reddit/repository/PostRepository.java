package fr.uge.reddit.repository;

import fr.uge.reddit.model.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostRepository<T extends Post> extends PagingAndSortingRepository<T, Long> {

    @Query("SELECT p FROM Post p LEFT JOIN FETCH p.scores WHERE p.id = :postId")
    Optional<Post> findPostById(Long postId);

}
