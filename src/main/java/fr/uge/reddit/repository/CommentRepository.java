package fr.uge.reddit.repository;

import fr.uge.reddit.model.Comment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CommentRepository extends PostRepository<Comment> {

    @Query("SELECT c FROM Comment c LEFT JOIN FETCH c.scores LEFT JOIN FETCH c.comments WHERE c.id = :commentId")
    Optional<Comment> findCommentById(Long commentId);
}
