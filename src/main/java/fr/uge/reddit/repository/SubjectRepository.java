package fr.uge.reddit.repository;

import fr.uge.reddit.model.Post;
import fr.uge.reddit.model.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectRepository extends PostRepository<Subject> {

    @Override
    @Query("select distinct a from Subject a LEFT JOIN FETCH a.scores LEFT JOIN FETCH a.comments WHERE a.id = :id")
    Optional<Subject> findById(Long id);

    @Query("select distinct a from Post a LEFT JOIN FETCH a.scores LEFT JOIN FETCH a.comments WHERE a.post_id = :id or a.id = :id")
    Optional<List<Post>> fetchChild(Long id);


    @Query(value = "Select  a from Subject a LEFT JOIN FETCH a.scores LEFT JOIN FETCH a.comments order by size(a.comments) desc",
            countQuery = "Select count (s) from Subject s")
    Page<Subject> findAllSortByNumberOfCommentDesc(Pageable pageable);

    @Query(value = "Select  a from Subject a LEFT JOIN FETCH a.scores LEFT JOIN FETCH a.comments order by size(a.comments) asc",
            countQuery = "Select count (s) from Subject s")
    Page<Subject> findAllSortByNumberOfCommentAsc(Pageable pageable);

    @Query(value = "SELECT distinct a from Subject a LEFT JOIN FETCH a.scores where a.id in :idList",
            countQuery = "Select count (s) from Subject s")
    Page<Subject> findAll(List<Long> idList, Pageable pageable);

    @Query(value = "SELECT a.id from Subject a order by a.id")
    List<Long> findAllId();
}
