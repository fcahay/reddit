package fr.uge.reddit.model;

import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "User")
public class User {
    /**
     * Nom d'utilisateur unique.
     */
    @Id
    private String username;

    /**
     * Mot de passe de l'utilisateur.
     */
    private String password;

    /**
     * Date à laquelle l'utilisateur a créé son compte.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date registerDate;
    @OneToMany(cascade = {CascadeType.PERSIST},fetch = FetchType.EAGER, mappedBy = "user")
    private Set<Post> posts;

    private String role;

    @Version
    Long version;


    public User(String username, String password, String role, Date date) {
        this.username = username;
        this.password = password;
        this.posts = new HashSet<>();
        this.role = role;
        this.registerDate = date;
    }

    public User(String username, String password) {
        this(username, password, "USER", new Date());
    }

    /**
     * Pour Hibernate et JPA
     */
    protected User() {

    }


    // GETTER

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public String getRole() {
        return role;
    }

    public Set<Subject> getSubjects() {
        // à voir si pas plus opti
        return posts.stream().filter(Subject.class::isInstance).map(Subject.class::cast).collect(Collectors.toSet());
    }

    public Set<Comment> getComments() {
        // à voir si pas plus opti
        return posts.stream().filter(Comment.class::isInstance).map(Comment.class::cast).collect(Collectors.toSet());
    }


    // SETTER

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }


    // PUBLIC METHODS

    public void addSubject(Post post) {
        posts.add(post);
    }

    public void encodePassword(PasswordEncoder passwordEncoder) {
        this.password = passwordEncoder.encode(this.password);
    }

    public boolean isAdmin(){
        return role.equals("admin");
    }

    public void updatePassword(String newPassword, PasswordEncoder passwordEncoder) {
        this.password = passwordEncoder.encode(newPassword);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User user) || username == null) return false;
        return username.equals(user.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
