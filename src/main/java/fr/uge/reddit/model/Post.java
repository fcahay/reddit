package fr.uge.reddit.model;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Classe représentant un post d'un utilisateur. Cela peut
 * être un sujet ou bien un commentaire.
 */
@Entity(name = "Post")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "post_type")
public class Post {

    @Id
    @GeneratedValue(generator = "postGenerator" ,strategy=GenerationType.IDENTITY)
    private Long id;

    /**
     * Contenu du commentaire.
     */
    @NotEmpty(message = "{form.comment.error.empty}") // TODO A revoir
    @Column(length = 2048) // TODO taille du contenu
    private String content;

    /**
     * Date et heure à laquelle le commentaire à été créé.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, mappedBy = "post")
    private Map<String,Score> scores;

    @OneToMany(cascade = {CascadeType.REMOVE}, orphanRemoval = true, mappedBy = "parent")
    private Set<Comment> comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "User_Name")
    private User user;

    private Long post_id;

    private int score;

    @Version
    Long version;


    protected Post(@NonNull String content, @NonNull Date timestampCreation, @NonNull HashMap<String,Score> scores, @NonNull Set<Comment> comments, @NonNull User user) {
        this.content = Objects.requireNonNull(content);
        this.scores = new HashMap<>();
        scores.forEach((key, value) -> addScore(value,key));
        this.comments = new HashSet<>();
        comments.forEach(this::addComment);
        this.user = Objects.requireNonNull(user);
        this.timestamp = Objects.requireNonNull(timestampCreation);
        this.score = 0;
    }

    /**
     * Pour Hibernate et JPA
     */
    protected Post() {

    }


    // GETTER

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getElapsedTime(){
        var tmp = new Date().getTime() - timestamp.getTime();
        if(tmp < 60 * 1000 ){
            return "Il y a " + TimeUnit.MILLISECONDS.toSeconds(tmp) + " secondes";
        }
        if(tmp < 60 * 60 * 1000 ){
            return "Il y a " + TimeUnit.MILLISECONDS.toMinutes(tmp) + " minutes";
        }
        if(tmp < 24 * 60 * 60 * 1000){
            return "Il y a " + TimeUnit.MILLISECONDS.toHours(tmp) + " heures";
        }
        if(tmp < 30 * 24 * 60 * 60 * 1000){
            return "Il y a " + TimeUnit.MILLISECONDS.toDays(tmp) + " jours";
        }
        if(tmp < 12 * 30 * 24 * 60 * 60 * 1000){
            return "Il y a " + TimeUnit.MILLISECONDS.toDays(tmp)/30 + " mois";
        }
        return "Il y a " + TimeUnit.MILLISECONDS.toDays(tmp)/365 + " ans";
    }

    public int getScore() {
        return scores.values().stream().mapToInt(x -> x.getScore() ? 1 : -1).sum();
    }

    public String getScoreLabel() {
        var score = getScore();
        if (score >= 0) {
            return "+" + score;
        }
        return "" + score;
    }

    public Map<String,Score> getScores() {
        return scores;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public User getUser() {
        return user;
    }

    public String getUsername() {
        if(user == null){
            return "anonyme";
        }
        return user.getUsername();
    }

    public Long getPost_id() {
        return post_id;
    }

    // SETTER

    public void setContent(String content) {
        this.content = content;
    }

    public void setTimestamp(Date timestampCreation) {
        this.timestamp = timestampCreation;
    }

    public void setScores(Map<String,Score> scores) {
        this.scores = scores;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPost_id(Long postId) {
        this.post_id = postId;
    }


    // PUBLIC METHODS

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Post post)) return false;
        return getId().equals(post.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    public void addScore(Score score, String username) {
        Objects.requireNonNull(score);
        scores.put(username, score);
        this.score = getScore();
    }

    public int updateScore(String username, boolean value) {
        Objects.requireNonNull(username);
        scores.compute(username, (key, oldValue) -> {
            if (oldValue == null) {
                return new Score(value, this); // si l'utilisateur n'a pas encore voté
            }
            oldValue.setScore(value); // si l'utilisateur a déjà voté mais qu'il change de vote
            return oldValue;
        });
        score = getScore(); // actualisation du score courant
        return score;
    }

    public void addComment(Comment comment) {
        Objects.requireNonNull(comment);
        comments.add(comment);
    }

    /**
     * Indique si un utilisateur a déjà ajouté un score sur un post et que son nouveau vote et identique à son ancien vote.
     * Cas possible si l'utilisateur n'utilise pas l'interface graphique et qu'il utilise l'api directement.
     *
     * @param username le nom de l'utilisateur qui vient de voté
     * @param newScore la valeur du vote : <code>true</code> = +1, <code>false</code> = -1
     * @return <code>true</code> si son vote est identique à l'ancien sinon <code>false</code>
     * @see Score
     */
    public boolean alreadyVotedAndSameVote(String username, boolean newScore) {
        var score = scores.get(username);
        if (score == null) {
            return false;
        }
        return score.getScore() == newScore;
    }


}
