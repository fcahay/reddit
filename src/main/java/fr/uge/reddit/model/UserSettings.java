package fr.uge.reddit.model;

import fr.uge.reddit.utils.Password;
import fr.uge.reddit.utils.Username;

/**
 * Classe utilisée lorsqu'un utilisateur modifie son mot de passe.
 */
public record UserSettings(@Username String username,
                           @Password String password) {
}
