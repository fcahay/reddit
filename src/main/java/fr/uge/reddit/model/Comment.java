package fr.uge.reddit.model;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

@Entity(name = "Comment")
@DiscriminatorValue("C")
public class Comment extends Post {

    @ManyToOne(fetch = FetchType.LAZY)
    private Post parent;
    private boolean state = true;


    public Comment(@NonNull String content, @NonNull User user, @NonNull Post parent) {
        super(content, new Date(), new HashMap<>(), new HashSet<>(), user);
        this.parent = Objects.requireNonNull(parent);
        setPost_id(parent.getPost_id());
    }

    /**
     * Pour Hibernate et JPA
     */
    public Comment() {

    }


    // GETTER

    public Post getParent() {
        return parent;
    }

    public boolean getState(){ return state; }

    @Override
    public String getContent() {
        if(state) {
            return super.getContent();
        }
        return "Contenu supprimé par l'auteur";
    }

    // SETTER

    public void setParent(Post parent) {
        this.parent = parent;
    }

    public void setState(boolean state){ this.state = state;}

}
