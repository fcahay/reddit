package fr.uge.reddit.model;

import javax.persistence.*;

@Entity
public class Score {
    @Id
    @GeneratedValue(generator = "scoreGenerator",strategy=GenerationType.IDENTITY)
    private Long id;

    /**
     * Représente la note : <code>true</code> pour un score positif et <code>false</code> pour un score négatif.
     */
    private boolean score;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private Post post;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "User_Name")
//    private User user;


    private Score(boolean score) {
        this.score = score;
    }

    public Score(boolean score, Post post) {
        this(score);
        this.post = post;
    }

    public Score() {

    }


    // GETTER

    public boolean getScore() {
        return score;
    }

    public Post getPost() {
        return post;
    }

//    public User getUser() {
//        return user;
//    }


    // SETTER


    public void setScore(boolean score) {
        this.score = score;
    }

    public void setPost(Post post) {
        this.post = post;
    }

//    public void setUser(User user) {
//        this.user = user;
//    }


    // PUBLIC METHODS


//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (id == null || o == null || getClass() != o.getClass()) return false;
//        Score score = (Score) o;
//        return post.equals(score.post) && user.equals(score.user);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(post, user);
//    }

    @Override
    public String toString() {
        return "Score{" +
                "id=" + id +
                ", score=" + score +
                ", post=" + post +
                '}';
    }
}
