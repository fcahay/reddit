package fr.uge.reddit.model;

import org.springframework.lang.NonNull;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

@Entity(name = "Subject")
@DiscriminatorValue("S")
public class Subject extends Post {

    /**
     * Titre principal du sujet.
     */
    private String title;


    public Subject(@NonNull String title, @NonNull String content, @NonNull User user) {
        super(content, new Date(), new HashMap<>(), new HashSet<>(), user);
        this.title = Objects.requireNonNull(title);
    }

    /**
     * Pour Hibernate et JPA
     */
    public Subject() {

    }


    // GETTER

    public String getTitle() {
        return title;
    }

    @Override
    public Long getPost_id() {
        return getId();
    }


    // SETTER

    public void setTitle(String title) {
        this.title = title;
    }


    // PUBLIC METHODS


}
