package fr.uge.reddit.model;

import fr.uge.reddit.utils.Password;
import fr.uge.reddit.utils.Username;

public class UserRegistration {

    @Username
    private String username;

    @Password
    private String password;

    @Password
    private String passwordConfirmation;

    public UserRegistration() { }

    public UserRegistration(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public boolean isSamePassword() {
        return password.equals(passwordConfirmation);
    }

}
