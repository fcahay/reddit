package fr.uge.reddit.controller;

import fr.uge.reddit.model.UserRegistration;
import fr.uge.reddit.service.UserService;
import fr.uge.reddit.utils.UserAlreadyExistException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class RegisterController {

    private final UserService userService;

    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registerAttribute", new UserRegistration());
        return "register";
    }

    @PostMapping("/register")
    public String registerFormProcess(@Valid @ModelAttribute("registerAttribute") UserRegistration userRegistration,
                                      BindingResult bindingResult,
                                      HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!userRegistration.isSamePassword()) {
            bindingResult.rejectValue("passwordConfirmation", "register.error.passwordConfirmation");
            return "register";
        }

        try {
            userService.createUser(userRegistration);
        } catch (UserAlreadyExistException exception) {
            bindingResult.rejectValue("username", "register.error.username");
            return "register";
        }

        try {
            // Connection automatique après une inscription
            request.login(userRegistration.getUsername(), userRegistration.getPassword());
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return "redirect:/home";
    }
}
