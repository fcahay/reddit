package fr.uge.reddit.controller;

import fr.uge.reddit.model.Comment;
import fr.uge.reddit.model.Subject;
import fr.uge.reddit.model.UserSettings;
import fr.uge.reddit.service.PostService;
import fr.uge.reddit.service.ScoreService;
import fr.uge.reddit.service.UserService;
import org.commonmark.node.AbstractVisitor;
import org.commonmark.node.Image;
import org.commonmark.node.Link;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashSet;

@Controller
public class HomeController {

    private final PostService postService;
    private final ScoreService scoreService;
    private final UserService userService;

    private final static Parser MARKDOWN_PARSER = Parser.builder().enabledBlockTypes(new HashSet<>()).build();
    private final static HtmlRenderer HTML_RENDERER = HtmlRenderer.builder()
            .escapeHtml(true).sanitizeUrls(true)
            .build();


    public HomeController(PostService postService, ScoreService scoreService, UserService userService) {
        this.postService = postService;
        this.scoreService = scoreService;
        this.userService = userService;
    }


    @GetMapping("/")
    public String redirectToHome() {
        return "redirect:/home";
    }

    /**
     * Affiche la page n°<code>page</code> contenant <code>nbPostByPage</code> sujets.
     *
     * @param model
     * @param page le numéro de la page à afficher
     * @param sortBy le type de tri à appliquer sur les sujets
     * @param orderBy l'ordre d'affichage des sujets ascendant ou descendant
     * @param nbPostByPage le nombre de sujets à afficher sur une page
     * @return
     * @throws IllegalAccessException
     */
    @GetMapping("/home")
    public String getHome(Model model,
                          @RequestParam(defaultValue = "0") int page,
                          @RequestParam(defaultValue = "score") String sortBy,
                          @RequestParam(defaultValue = "desc") String orderBy,
                          @RequestParam(defaultValue = "10") int nbPostByPage) throws IllegalAccessException {
        var order = orderBy.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        var results = postService.sortByType(page, nbPostByPage, sortBy, order);
        model.addAttribute("results", results);
        model.addAttribute("subjectAttribute", new Subject());
        return "home";
    }

    /**
     * Affiche une page de confirmation avant de se rendre vers un autre site internet.
     *
     * @param url l'URL vers lequel l'utilisateur va être redirigé
     * @param model
     * @return
     */
    @GetMapping("/exit")
    public String exit(@RequestParam(required = false) String url, Model model) {
        if (url == null) {
            return "redirect:/";
        }
        model.addAttribute("url", url);

        return "exit";
    }

    /**
     * Affiche tous les commentaires d'un sujet en particulier.
     *
     * @param model
     * @param idSubject
     * @return
     */
    @GetMapping("/home/s/{idSubject}")
    public String getSubjectDetails(Model model, @PathVariable Long idSubject) {
        var subjectOptional = postService.fetchChildren(idSubject);

        subjectOptional.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "sujet introuvable"));
        var tmp = subjectOptional.get();
        var post = (Subject) tmp.stream().filter(x -> x.getId().equals(idSubject)).toList().get(0);

        model.addAttribute("post", post);
        model.addAttribute("commentContent", new Comment());
        return "comments";
    }

    /**
     * Renvoie un badge boostrap.
     *
     * @param model
     * @param id
     * @param score
     * @return
     */
    @GetMapping("/home/fragments/badge")
    public String getBadgeFragment(Model model, @RequestParam int id, @RequestParam int score) {
        model.addAttribute("isPositive", score >= 0);
        model.addAttribute("idPost", id);
        model.addAttribute("label", score);
        return "fragments :: badgeScore";
    }

    /**
     * Affiche la page d'un utilisateur, elle contient les sujets et commentaires
     * créés par ce dernier.
     *
     * @param model
     * @param username
     * @return
     */
    @GetMapping("/home/u/{username}")
    public String getUserProfile(Model model, @PathVariable String username) {
        var user = userService.getUserByUsername(username);

        user.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "utilisateur introuvable"));

        model.addAttribute("user", user.get());
        model.addAttribute("settings", new UserSettings(username, ""));
        model.addAttribute("subjectAttribute", new Subject());
        return "profile";
    }

    /**
     * Si une <code>RuntimeException</code> est levée lors de l'exécution du site, on redirige
     * vers une page d'erreur.
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public String error(RuntimeException exception) {
        System.err.println(exception.getMessage());
        return "error";
    }

    @PostMapping("/user/s/{idSubject}/{idComment}/comment")
    public String createCommentFromComment(Principal principal, @PathVariable long idSubject, @PathVariable long idComment,
                                           @Valid @ModelAttribute("commentContent") Comment comment,
                                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/home/s/" + idSubject; // TODO afficher les erreurs sur le front
        }

        var commentParent = postService.getCommentById(idComment);
        commentParent.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "commentaire introuvable"));

        var user = userService.getUserByUsername(principal.getName());
        user.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "utilisateur introuvable"));

        var result = parseMarkdownToHtml(comment.getContent());

        postService.createComment(user.get(), commentParent.get(), result);

        return "redirect:/home/s/" + idSubject;
    }

    @PostMapping("/user/s/{idSubject}/comment")
    public String createCommentFromSubject(Principal principal, @PathVariable long idSubject,
                                           @Valid @ModelAttribute("commentContent") Comment comment,
                                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/home/s/" + idSubject; // TODO afficher les erreurs sur le front
        }

        var subject = postService.getSubjectById(idSubject);
        subject.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "sujet introuvable"));

        var user = userService.getUserByUsername(principal.getName());
        user.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "utilisateur introuvable"));

        var result = parseMarkdownToHtml(comment.getContent());

        postService.createComment(user.get(), subject.get(), result);

        return "redirect:/home/s/" + idSubject;
    }

    @PostMapping("/user/s/new")
    public String createSubject(Principal principal,
                                @Valid @ModelAttribute("subjectAttribute") Subject subject,
                                BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "home"; // TODO : avertir l'utilisateur que la création n'a pas fonctionnée
        }

        var result = parseMarkdownToHtml(subject.getContent());

        postService.createSubject(principal.getName(), subject.getTitle(), result);
        return "redirect:/home";
    }

    /**
     * Transforme du texte en markdown vers de l'HTML.
     * <p>
     * Note :
     * <ul>
     *     <li>Prend en charge seulement le gras, l'italique et les liens</li>
     *     <li>Supprime les url des images car on n'accepte pas les images</li>
     *     <li>Modifie l'url d'un lien afin de proposer une confirmation de redirection lors de son clic</li>
     * </ul>
     *
     * @param content le texte en markdown
     * @return le texte transformé en HTML
     */
    private String parseMarkdownToHtml(String content) {
        var document = MARKDOWN_PARSER.parse(content);
        document.accept(new AbstractVisitor() {
            @Override
            public void visit(Image image) {
                super.visit(image);
                image.setDestination("");
            }

            @Override
            public void visit(Link link) {
                super.visit(link);
                link.setDestination(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString() + "/?url=" + link.getDestination());
            }
        });
        return HTML_RENDERER.render(document);
    }

    @GetMapping("/admin/{idSubject}")
    public String deleteSubject(@PathVariable long idSubject){
        postService.removeSubject(idSubject);
        return "redirect:/home";
    }

}
