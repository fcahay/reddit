package fr.uge.reddit.controller;

import fr.uge.reddit.model.UserSettings;
import fr.uge.reddit.service.PostService;
import fr.uge.reddit.service.ScoreService;
import fr.uge.reddit.service.UserService;
import fr.uge.reddit.utils.InputError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.stream.Collectors;

@RestController
public class ActionController {

    private final PostService postService;
    private final ScoreService scoreService;
    private final UserService userService;

    public ActionController(PostService postService, ScoreService scoreService, UserService userService) {
        this.postService = postService;
        this.scoreService = scoreService;
        this.userService = userService;
    }


    @PostMapping("/user/like/{idPost}")
    public ResponseEntity<?> likePost(Principal principal, @PathVariable long idPost, @RequestParam boolean flag) {
        if (principal == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "utilisateur non authentifié");
        }
        return scoreService.createPostScore(flag, principal.getName(), idPost);
    }

    @PutMapping("/user/settings")
    public ResponseEntity<?> updatePassword(@Valid @RequestBody UserSettings userSettings, BindingResult bindingResult, Principal principal) {
        if (!userSettings.username().equals(principal.getName())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "seul l'utilisateur concerné peut modifier son mot de passe");
        }
        if (bindingResult.hasErrors()) {
            var error = new InputError(bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList()));
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(error);
        }

        return userService.updatePassword(userSettings.username(), userSettings.password());
    }

    @DeleteMapping("/user/c")
    public ResponseEntity<?> deleteComment(Principal principal, @RequestParam int id) {
        if (!postService.removeComment(id, principal.getName())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Seul le propriétaire du commentaire peut le supprimer");
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }


}
