package fr.uge.reddit.utils;

import java.io.Serial;

public class PostNotFoundException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1799283919677296068L;

    public PostNotFoundException() {
        super();
    }

    public PostNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PostNotFoundException(final String message) {
        super(message);
    }

    public PostNotFoundException(final Throwable cause) {
        super(cause);
    }
}
