package fr.uge.reddit.utils;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;

@Component
public class RedirectionFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        var req = (HttpServletRequest) request;
        var resp = (HttpServletResponse) response;

        var url = request.getParameter("url");
        var baseDomain = req.getHeader("host");

        if (url != null) {
            if (isSameDomain(url, baseDomain)) {
                resp.sendRedirect(url);
            } else {
                req.getRequestDispatcher("/exit").forward(req, resp);
            }
            return;
        }

        chain.doFilter(request, response);
    }

    private boolean isSameDomain(String url, String baseDomain) {
        var urlDest = URI.create(url);
        return baseDomain.equals(urlDest.getAuthority());
    }
}
