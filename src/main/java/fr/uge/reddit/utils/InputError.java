package fr.uge.reddit.utils;

import java.io.Serializable;
import java.util.List;

public record InputError(List<String> errors) implements Serializable {
}
