package fr.uge.reddit.utils;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.lang.annotation.*;

/**
 * La chaîne de caractères annotée doit correspondre à un mot de passe valide,
 * c'est à dire qu'elle doit contenir seulement des caractères alphanumériques,
 * des caractères spéciaux (.?!,;:*@$%#) et avoir une longueur entre 5 et 25.
 *
 * @see Pattern
 * @see Size
 */
@Pattern(regexp = "[A-Za-z0-9?&!*@$%#]*", message = "{error.validation.password.regex}")
@Size(min = 5, max = 25, message = "{error.validation.password.size}")
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { })
public @interface Password {

    /**
     * @return the error message template
     */
    String message() default "";

    /**
     * @return the groups the constraint belongs to
     */
    Class<?>[] groups() default { };

    /**
     * @return the payload associated to the constraint
     */
    Class<? extends Payload>[] payload() default { };
}
