package fr.uge.reddit.service;

import fr.uge.reddit.model.Comment;
import fr.uge.reddit.model.Post;
import fr.uge.reddit.model.Subject;
import fr.uge.reddit.model.User;
import fr.uge.reddit.repository.CommentRepository;
import fr.uge.reddit.repository.PostRepository;
import fr.uge.reddit.repository.SubjectRepository;
import fr.uge.reddit.utils.PostNotFoundException;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {
    private final SubjectRepository subjectRepository;
    private final CommentRepository commentRepository;
    private final PostRepository<Post> postRepository;

    @PersistenceUnit
    EntityManagerFactory emf;

    @PersistenceContext
    EntityManager em;

    public PostService(SubjectRepository subjectRepository, CommentRepository commentRepository, PostRepository<Post> postRepository) {
        this.subjectRepository = subjectRepository;
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }


    @Transactional
    public Comment createComment(User user, Post parent, String content) {
        var com = new Comment(content, user, parent);
        em.persist(com);
        return com;
    }

    @Transactional
    public void createSubject(String username, String title, String content) {
        var user = em.find(User.class, username);
        if (user == null) {
            throw new UsernameNotFoundException(username + " not found.");
        }
        var subjectToCreate = new Subject(title, content, user);
        em.persist(subjectToCreate);
    }

    public Page<Subject> getAllSubject(int page, int size) {
        return subjectRepository.findAll(PageRequest.of(page, size));
    }

    @Transactional
    public boolean removeComment(long commentId, String username){
        var tmp = commentRepository.findCommentById(commentId);
        tmp.orElseThrow(() -> new PostNotFoundException("comment ("+commentId+") not found"));

        var comment = tmp.get();
        if (!comment.getUsername().equals(username)) {
            return false;
        }
        comment.setState(false);
        return true;
    }

    @Transactional
    public boolean removeSubject(long subjectId){
        var tmp = subjectRepository.findById(subjectId);
        if(tmp.isEmpty()){
            return false;
        }
        subjectRepository.delete(tmp.get());
        return true;
    }

    @Transactional
    public Optional<Subject> getSubjectById(long id) {
        return subjectRepository.findById(id);
    }

    @Transactional
    public Optional<List<Post>> fetchChildren(Long id) {
        return subjectRepository.fetchChild(id);
    }

    public Optional<Post> getPostById(long id) {
        return postRepository.findPostById(id);
    }

    public Optional<Comment> getCommentById(long id) {
        return commentRepository.findCommentById(id);
    }

    public PagedListHolder<Subject> sortByType(int page, int size,String sortType,Sort.Direction order) throws IllegalAccessException {
        return switch (sortType) {
            case "score", "timestamp" -> findAll(page, size, sortType, order);
            default -> throw new IllegalAccessException("argument " + sortType + "not recognised");
        };
    }

    public PagedListHolder<Subject> findAll(int page, int size, String sortType, Sort.Direction order){
        List<Long> idList = em.createQuery("SELECT s.id from Subject s", Long.class).getResultList();
        var subjects = em.createQuery("SELECT distinct a from Subject a LEFT JOIN FETCH a.scores where a.id in :idList", Subject.class)
                .setParameter("idList", idList).getResultList();
        var sort = new MutableSortDefinition(sortType, false, order.isAscending());
        var res = new PagedListHolder<>(subjects, sort);
        res.resort();
        res.setPageSize(size);
        res.setPage(page);
        return res;
    }
}
