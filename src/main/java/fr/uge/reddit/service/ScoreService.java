package fr.uge.reddit.service;

import fr.uge.reddit.model.User;
import fr.uge.reddit.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

@Service
public class ScoreService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UpdateScoreWithFailure updateScoreWithFailure;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @PersistenceContext
    private EntityManager em;


    public ResponseEntity<?> createPostScore(boolean flag, String username, long postId) {
        var user = userRepository.findByUsername(username); //em.find(User.class, username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException(username + " not found.");
        }
        var retry=true;
        ResponseEntity<?> result = null;
        while(retry) {
            retry=false;
            try {
                result = updateScoreWithFailure.updateScore(flag,username,postId);
            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e){
                retry=true;
            }
        }
        return result;
    }


}
