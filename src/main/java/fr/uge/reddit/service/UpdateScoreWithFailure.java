package fr.uge.reddit.service;

import fr.uge.reddit.model.Post;
import fr.uge.reddit.repository.PostRepository;
import fr.uge.reddit.repository.ScoreRepository;
import fr.uge.reddit.utils.PostNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UpdateScoreWithFailure {
    @Autowired
    PostRepository<Post> postRepository;

    @Transactional
    public ResponseEntity<?> updateScore(boolean flag, String username, long postId){
        var post = postRepository.findPostById(postId);
        if (post.isEmpty()) {
            throw new PostNotFoundException("post with id (" +postId+ ") not found");
        }
        var res = post.get();
        if (res.alreadyVotedAndSameVote(username, flag)) {
            // le nouveau vote de l'utilisateur est identique à son ancien vote
            return ResponseEntity.badRequest().build();
        }
        var tmp = res.updateScore(username, flag);
//        postRepository.save(res);
        return ResponseEntity.ok(tmp);
    }
}
