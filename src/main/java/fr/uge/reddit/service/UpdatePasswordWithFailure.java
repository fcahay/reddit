package fr.uge.reddit.service;

import fr.uge.reddit.model.Post;
import fr.uge.reddit.repository.PostRepository;
import fr.uge.reddit.repository.UserRepository;
import fr.uge.reddit.utils.PostNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UpdatePasswordWithFailure {
    @Autowired
    UserRepository userRepository;

    @Transactional
    public ResponseEntity<?> updatePassword(String username, String newPassword, PasswordEncoder passwordEncoder){
        var userOptional = userRepository.findByUsername(username);
        userOptional.orElseThrow(() -> new UsernameNotFoundException(username + " not found."));

        var res = userOptional.get();
        res.updatePassword(newPassword, passwordEncoder);
        return ResponseEntity.ok(userRepository.save(res));
    }
}
