package fr.uge.reddit.service;

import fr.uge.reddit.model.User;
import fr.uge.reddit.model.UserDetailsImpl;
import fr.uge.reddit.model.UserRegistration;
import fr.uge.reddit.repository.UserRepository;
import fr.uge.reddit.utils.UserAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Autowired
    private UpdatePasswordWithFailure updatePasswordWithFailure;


    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    private void encodePassword(User user) {
        user.encodePassword(passwordEncoder);
    }

    @Transactional
    public void createUser(UserRegistration userRegistration) throws UserAlreadyExistException {
        var userToCreate = userRepository.findByUsername(userRegistration.getUsername());
        if (userToCreate.isEmpty()) {
            var user = new User(userRegistration.getUsername(), userRegistration.getPassword());
            encodePassword(user);
            userRepository.save(user);
        } else {
            throw new UserAlreadyExistException("There is already an account with this username : " + userRegistration.getUsername());
        }
    }

    @Transactional
    public void createUser(User user) throws UserAlreadyExistException {
        var userToCreate = userRepository.findByUsername(user.getUsername());
        if (userToCreate.isEmpty()) {
            encodePassword(user);
            userRepository.save(user);
        } else {
            throw new UserAlreadyExistException("There is already an account with this username : " + user.getUsername());
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findByUsername(username);

        user.orElseThrow(() -> new UsernameNotFoundException(username + " not found."));

        return user.map(UserDetailsImpl::new).get();
    }

    @Transactional
    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional
    public ResponseEntity<?> updatePassword(String username, String newPassword) {
        var retry = true;
        ResponseEntity<?> result = null;
        while(retry) {
            retry = false;
            try {
                result = updatePasswordWithFailure.updatePassword(username, newPassword, passwordEncoder);
            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e){
                retry = true;
            }
        }
        return result;
    }

}
