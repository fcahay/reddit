package fr.uge.reddit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ADMIN = "ADMIN";
    private static final String USER = "USER";

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) {
        // On indique à Spring Security d'ignorer et de laisser passer les requêtes
        // vers les dossier /css, /js et /images ainsi que leurs fichiers.
        // https://stackoverflow.com/questions/41910004/add-css-file-to-spring-boot-spring-security-thymeleaf-file
        web.ignoring().antMatchers("/css/**", "/js/**", "/images/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/admin/**").hasAuthority(ADMIN)
                .antMatchers("/user/**").hasAnyAuthority(ADMIN, USER)
                .antMatchers("/", "/home/**").permitAll()
                .antMatchers("/h2-console/**").permitAll() // A RETIRER pour la prod
                .antMatchers("/exit").permitAll()
                .antMatchers("/register").permitAll()
                .anyRequest().authenticated()
            .and()
                .formLogin()
                .loginPage("/login").defaultSuccessUrl("/", true).permitAll()
            .and()
                .logout().permitAll();

        // Permet d'accèder à la console h2 depuis le navigateur
        // car Spring Security bloque l'accès.
        // A RETIRER pour la prod
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }

}
