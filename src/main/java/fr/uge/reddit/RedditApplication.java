package fr.uge.reddit;

import com.github.javafaker.Faker;
import fr.uge.reddit.model.User;
import fr.uge.reddit.model.UserRegistration;
import fr.uge.reddit.service.PostService;
import fr.uge.reddit.service.ScoreService;
import fr.uge.reddit.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@SpringBootApplication
public class RedditApplication {
    private final static int NUM_COMMENT = 1000;
    private final static int NUM_USER = 10;
    private final static int NUM_SCORE = 100;
    private final static int NUM_SUBJECT = 1000;

    /**
     * Génère une liste d'utilisateurs représentant le jeu de données de test.
     *
     * @param userService
     * @param faker le générateur pseudo-aléatoire de fausses données
     * @return la liste des utilisateurs créés
     */
    public static List<String> generateUsers(UserService userService, Faker faker){
        var nameList = new ArrayList<String>();
        for (int i = 0; i < NUM_USER; i++) {
            var tmp = new UserRegistration();
            tmp.setUsername(faker.name().firstName().replaceAll("'", "").toLowerCase() + "_" + i);
            tmp.setPassword(faker.internet().password(5, 25, true, true, true));
            userService.createUser(tmp);
            nameList.add(tmp.getUsername());
        }
        userService.createUser(new UserRegistration("arnaud", "arnaud"));
        userService.createUser(new User("admin", "admin", "ADMIN", new Date()));
        return nameList;
    }

    /**
     * Génère un ensemble de sujets représentant le jeu de données de test.
     *
     * @param postService
     * @param faker le générateur pseudo-aléatoire de fausses données
     * @param rand
     * @param nameList une liste de noms d'utilisateurs existants
     */
    public static void generateSubjects(PostService postService, Faker faker, Random rand, List<String> nameList){
        for(int i = 0; i < NUM_SUBJECT; i++){
            postService.createSubject(
                    nameList.get(rand.nextInt(0, NUM_USER)),
                    faker.book().title(),
                    faker.shakespeare().hamletQuote());
        }
    }

    /**
     * Génère un ensemble de commentaires représentant le jeu de données de test.
     *
     * @param userService
     * @param postService
     * @param faker le générateur pseudo-aléatoire de fausses données
     * @param rand
     * @param nameList une liste de noms d'utilisateurs existants
     */
    public static void generateComments(UserService userService, PostService postService, Faker faker, Random rand, List<String> nameList){
        for(int i =0; i < NUM_COMMENT; i++){
            var user = userService.getUserByUsername(nameList.get(rand.nextInt(0, NUM_USER)));
            var post = postService.getPostById(rand.nextInt(0,NUM_SUBJECT + i));
            if(user.isEmpty() || post.isEmpty()) {
                continue;
            }
            postService.createComment(user.get(), post.get(), faker.harryPotter().quote());
        }
    }

    /**
     * Génère un ensemble de score sur les sujets et commentaires représentant le jeu de données de test.
     *
     * @param scoreService
     * @param faker le générateur pseudo-aléatoire de fausses données
     * @param rand
     * @param nameList une liste de noms d'utilisateurs existants
     */
    public static void generateScores(ScoreService scoreService, Faker faker, Random rand, List<String> nameList){
        var inter = NUM_COMMENT/100;
        for(int i =0; i < NUM_USER; i++){
            for(int j = 0; j < NUM_SCORE; j++){
                scoreService.createPostScore(faker.bool().bool(),
                        nameList.get(i),
                        rand.nextInt(j*inter + 1,(j+1)*inter));
            }
        }
    }

    @Bean
    public CommandLineRunner cmd(UserService userService, ScoreService scoreService, PostService postService) {
        return args -> {
            var faker = new Faker();
            var rand = new Random();
            var nameList = generateUsers(userService, faker);
            generateSubjects(postService, faker, rand, nameList);
            generateComments(userService, postService, faker, rand, nameList);
            generateScores(scoreService, faker, rand, nameList);
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(RedditApplication.class, args);
    }

}
