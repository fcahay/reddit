# Reddit

Le but du projet est de réaliser un forum de discussion qui sera une version simplifiée du site [Reddit](https://www.reddit.com/).

## Groupe

- Florian CAHAY
- Vincent LIN
- Kosal LUC

## Tech

- Spring
  - Spring Data
  - Spring MVC
  - Spring Security
  - Spring Boot
- Thymeleaf
- H2 Database
- Hibernate
- Maven